# <p align="center"> M I N X </p>
MINX is a MNase-seq processing pipeline written in Nextflow for single-stranded data. To get more information on parameters open up a terminal and type in `nextflow run minx-nf --help`.
