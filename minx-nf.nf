params.mismatches = 1

// print help usage
if (params.help) {
	log.info ''
	log.info '=============================='
	log.info '           M I N X            '
	log.info '=============================='
	log.info 'A MNase-seq processing pipeline'
	log.info ''
	log.info 'Run: '
	log.info '    minx-nf.nf --config --index [OPTIONS]...'
	log.info ''
	log.info 'General Options: '
	log.info '    --help               Show this message and exit.'
	log.info '    --config             A TAB seperated file containing metadata information.'
	log.info '    --index              An appropriate index file for Bowtie2.'
	log.info '    --threads            The number of threads cipher should use. (Default: 1)'
	log.info ''
	log.info 'Mapping Options: '
	log.info '    --mismatches         Number of mismatches allowed by Bowtie2'
	exit 1
}

config = file(params.config)

// Check input parameters
if (!params.config) {
	exit 1, "Please specify a config file"
}

if (!params.index) {
    exit 1, "Please specify an index file"
}

// End of input parameter check

// Open up a fastq channel
fastqs = Channel
.from(config.readLines())
.map { line ->
	list = line.split()
	sampleID = list[0]
	path = list[1]
	controlID = list[2]
	mark = list[3]
	[ sampleID, path, controlID, mark ]
}

// Step 1. Mapping

process mapping {

	input:
	set prefix, file(fastq), controlID, mark from fastqs

	output:
	set prefix, file("${prefix}.sorted.bam"), controlID, mark into bams

	script:
	"""
	bowtie2 -q -N ${params.mismatches} -p ${params.threads} -x ${params.index} -U ${fastq} | samtools view -b -F 3844 -q 5 - | samtools sort -O bam -@ ${params.threads} > ${prefix}.sorted.bam
	"""
}

// Step 2. Convert to bed

process bam2bed {

	input:
	set prefix, file(bam), controlID, mark from bams

	output:
	set prefix, file("${prefix}.sorted.bed"), controlID, mark into beds

	script:
	"""
	bedtools bamtobed -i ${bam} > ${prefix}.sorted.bed
	"""
}

// Step 3. Call peaks using iNPS

process peak_calling {

	input:
	set prefix, file(bed), controlID, mark from beds

	output:
	set prefix, file("${prefix}_peaks.bed"), controlID, mark into peaks

	script:
	"""
	python iNPS_V1.2.2.py -i ${bed} -o iNPS_peaks --s_p s
	"""
}
